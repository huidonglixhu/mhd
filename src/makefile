SHELL=/bin/bash
# -*- mode: makefile -*-
all: mhd

#CPPFLAGS = ${LAND_INC}
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
#include ${SLEPC_DIR}/lib/slepc/conf/slepc_common

# list of objects for the main program
MAIN_OBJS = mhd.o

mhd: ${MAIN_OBJS} chkopts
	-${CLINKER} -o $@ ${MAIN_OBJS} ${PETSC_LIB}
	-${DSYMUTIL} $@
	${RM} -f $(MAIN_OBJS)

xgcdriver: xgcdriver.o chkopts
	-${CLINKER} -o $@ xgcdriver.o ${PETSC_LIB}
	-${DSYMUTIL} $@
	${RM} -f xgcdriver.o

null: nullspace.o chkopts
	-${CLINKER} -o $@ nullspace.o ${PETSC_LIB} 
	-${DSYMUTIL} $@
	${RM} -f nullspace.o

NP=1
ORDER=2
REFINE=3
MEM=0
DT=1.e-3

VALGRIND_PROG=valgrind
VALGRIND_ARGS=--dsymutil=yes --leak-check=full --gen-suppressions=no --num-callers=20 --error-limit=no
VAL=$(VALGRIND_PROG) $(VALGRIND_ARGS)

run:
	-${MPIEXEC} -n ${NP} ./mhd -debug 1 -dim 2 -dm_refine ${REFINE} -ts_monitor -ts_type arkimex -ts_arkimex_type 1bee -ts_max_snes_failures -1 -ts_adapt_monitor -ts_atol 1e-2 -ts_adapt_clip .5,1.2 -ts_rtol 1e-2 -pc_type lu -pc_factor_mat_solver_type mumps -ksp_type preonly -snes_monitor -snes_rtol 1.e-12 -snes_stol 1.e-12 -snes_converged_reason -snes_atol 1.e-14 -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_max_time 3.9e-07 -ts_dt ${DT} -eps 1.e-12 -eta 0.001 -ves 0.005 -beta 0.01 -mu 0.0002 -y_periodicity PERIODIC -cells 2,6 -Jop 4.99 -snes_max_it 10 ${EXTRA_OPTIONS} -ts_max_steps 1 -dm_plex_periodic_cut -fft_line_dir 1 -fft_line_coord 1.0 -print_fft 2 -fft_component 3 -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append
# -dm_plex_periodic_cut -line_dir 1 -line_coord 1.0 -print_fft 2 -real_view :u.m:ascii_matlab -fft_view :spectra.m:ascii_matlab -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append 
	-@${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py sol.h5
#-line_coord 3.14159265359,1.57079632679

spectrum:
	-${MPIEXEC} -n ${NP} ./mhd -debug 1 -dim 2 -dm_refine ${REFINE} -ts_monitor -ts_type beuler -ts_max_snes_failures 1 -ts_adapt_monitor -ts_atol 1e-2 -ts_adapt_clip .5,1.2 -ts_rtol 1e-2 -pc_type none -ksp_type gmres -ksp_rtol 1.e-12 -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_dt 1000 -eps 1.e-12 -eta 0.001 -ves 0.005 -beta 0.01 -mu 0.0002 -y_periodicity PERIODIC -cells 2,6 -Jop 4.99 -snes_max_it 1 ${EXTRA_OPTIONS} -ts_max_steps 5 -ksp_gmres_restart 200 -ksp_max_it 200 -ksp_monitor_singular_value -ksp_plot_eigenvalues  -draw_pause -1   -ksp_view_eigenvalues

runexp:
	-@${MPIEXEC} -n ${NP} ./mhd -debug 1 -dim 2 -debug 1 -dim 2 -dm_refine ${REFINE} -ts_monitor -implicit false -ts_type ssp  -ts_adapt_monitor -ts_atol 1e-2 -ts_adapt_clip .5,1.2 -ts_rtol 1e-2 -pc_type lu -pc_factor_mat_solver_type mumps -ksp_type preonly -snes_monitor -snes_rtol 1.e-12 -snes_stol 1.e-12 -snes_converged_reason -snes_atol 1.e-14 -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_max_time 0.045 -ts_dt ${DT} -eps 1.e-12 -eta 0.001 -ves 0.005 -beta 0.01 -mu 0.0002 -y_periodicity PERIODIC -cells 2,6 -Jop 4.99 -snes_max_it 10 ${EXTRA_OPTIONS} -ts_max_steps 100 -dm_plex_periodic_cut -dm_view hdf5:exp.h5 -vec_view hdf5:exp.h5::append
	-${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py exp.h5

# -y_periodicity PERIODIC  -snes_type test -snes_test_display

val:
	-${MPIEXEC} -n ${NP} $(VAL) ./mhd -debug 1 -dim 2 -dm_refine ${REFINE} -ts_monitor -ts_type beuler -ts_arkimex_type 1bee -ts_max_snes_failures -1 -ts_adapt_monitor -ts_atol 1e-2 -ts_rtol 1e-2 -ts_adapt_clip .2,2 -ts_atol 1e-2 -ts_rtol 1e-2 -ts_adapt_clip .2,2 -pc_type lu -pc_factor_mat_solver_type mumps -ksp_type preonly -snes_monitor -snes_rtol 1.e-8 -snes_stol 1.e-8 -snes_converged_reason -snes_atol 1.e-14 -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_max_steps 4 -ts_dt ${DT} -eps 1.e-12 -eta 0.001 -ves 0.005 -beta 0.01 -mu 0.0002 -y_periodicity PERIODIC -cells 2,6 -Jop 4.99 -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append -snes_max_it 10 ${EXTRA_OPTIONS} -dm_plex_periodic_cut

#-on_error_attach_debugger lldb

lldb:
	-@lldb ./mhd -- -debug 1 -dim 2 -dm_refine_hierarchy ${REFINE} -ts_monitor -ts_type arkimex -ts_arkimex_type 1bee -ts_max_snes_failures -1 -ts_adapt_monitor -ts_atol 1e-2 -ts_rtol 1e-2 -ts_adapt_clip .2,2 -pc_type lu -pc_factor_mat_solver_type mumps -ksp_type preonly -snes_monitor -snes_rtol 1.e-10 -snes_stol 1.e-10 -snes_converged_reason -snes_atol 1.e-14 -snes_converged_reason -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_max_steps 1 -ts_dt ${DT} -eps 1.e-12 -eta 0.001 -ves 0.005 -beta 0.01 -mu 0.0002 -y_periodicity PERIODIC -cells 1,3 -Jop 4.99 -fft_line_dir 1 -fft_line_coord 3.14159265359 -real_view :u.m:ascii_matlab -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append 
	-@${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py *.h5


runnull:
	-${MPIEXEC} -n ${NP} ./null -debug 1 -dim 2 -dm_refine 0 -ts_monitor -ts_type arkimex -ts_arkimex_type 1bee -ts_max_snes_failures -1 -ts_adapt_monitor -ts_atol 1e-2 -ts_rtol 1e-2 -ts_adapt_clip .2,2 -pc_type lu -pc_factor_mat_solver_type mumps -ksp_type preonly -snes_monitor -snes_rtol 1.e-8 -snes_stol 1.e-8 -snes_converged_reason -snes_atol 1.e-14 -petscspace_degree ${ORDER} -petscspace_poly_tensor -ts_max_steps 1 -ts_dt ${DT} -eps 1.e-12 -eta 100.0 -ves 0.005 -beta 0.01 -mu 0.0002 -x_periodicity PERIODIC -y_periodicity PERIODIC -cells 3,3 -Jop 4.99 -eps_smallest_magnitude -eps_nev 10 -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append -mat_view :a.m:ascii_matlab 
	-@${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py *.h5
# -snes_test_jacobian -dm_plex_periodic_cut  -dm_view hdf5:sol.h5 -vec_view hdf5:sol.h5::append -dm_plex_periodic_cut 
# -snes_test_jacobian_display -snes_test_jacobian -mat_view :a.m:ascii_matlab 
# -dm_view hdf5:kernel.h5 -kernel_vec_view hdf5:kernel.h5::append
