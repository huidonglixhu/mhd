static char help[] = "Evolution of magnetic islands.\n\
The aim of this model is to self-consistently study the interaction between the tearing mode and small scale drift-wave turbulence.\n\n\n";

/*F
This is a three field model for the density $\tilde n$, vorticity $\tilde\Omega$, and magnetic flux $\tilde\psi$, using auxiliary variables potential $\tilde\phi$ and current $j_z$.
\begin{equation}
  \begin{aligned}
    \partial_t \tilde n       &= \left\{ \tilde n, \tilde\phi \right\} + \beta \left\{ j_z, \psi_0 + \tilde\psi \right\} + \left\{ \ln n_0, \tilde\phi \right\} + \mu \nabla^2_\perp \tilde n \\
  \partial_t \tilde\Omega   &= \left\{ \tilde\Omega, \tilde\phi \right\} + \beta \left\{ j_z, \psi_0 + \tilde\psi \right\} + \mu \nabla^2_\perp \tilde\Omega \\
  \partial_t \tilde\psi     &= \left\{ \psi_0 + \tilde\psi, \tilde\phi - \tilde n \right\} - \left\{ \ln n_0, \tilde\psi \right\} + \frac{\eta}{\beta} \nabla^2_\perp \tilde\psi \\
  \nabla^2_\perp\tilde\phi        &= \tilde\Omega \\
  j_z  &= -\nabla^2_\perp  \left(\tilde\psi + \psi_0  \right)\\
  \end{aligned}
\end{equation}
We further define parametric constants
\begin{equation}
  \begin{aligned}
    ves\equiv\frac{\partial \ln n_0}{\partial x}
  \end{aligned}
\end{equation}
Put in the $\Delta^\prime$ definition/derivation and the inner layer model, the linear drift wave  dispersion relation, and some nonlinear info from Spencer's paper.
F*/

#include <petscdmplex.h>
#include <petscts.h>
#include <petscds.h>
#include <assert.h>

#define MHD_NUM_COMP 5
#define MHD_MAX_SPECTRAL 16
typedef struct {
  PetscInt       debug;             /* The debugging level */
  /* Domain and mesh definition */
  PetscInt       dim;               /* The topological mesh dimension */
  char           filename[2048];    /* The optional ExodusII file */
  PetscBool      cell_simplex;           /* Simplicial mesh */
  DMBoundaryType boundary_types[3];
  PetscInt       cells[3];
  PetscInt       refine;
  /* geometry  */
  PetscReal      domain_lo[3], domain_hi[3];
  DMBoundaryType periodicity[3];              /* The domain periodicity */
  PetscReal      b0[3]; /* not used */
  /* Problem definition */
  PetscErrorCode (**initialFuncs)(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx);
  PetscReal      mu, eta, beta, ves;
  PetscReal      Jo,Jop,m,ke,kx,ky,DeltaPrime,eps;
  /* solver */
  PetscBool      implicit;
  PetscReal      dt; /* initial time step to not go over */
  PetscReal      dt_reduction_factor;
  PetscInt       iteration_dt_trigger;
  /* spectral meta-data */
  PetscInt       print_fft;         /* (Matlab) view spectra */
  PetscInt       fft_line_dir[3];
  PetscReal      fft_line_coord[MHD_MAX_SPECTRAL][2];
  PetscInt       fft_num_lines;
  PetscInt       fft_component;
} AppCtx;

static AppCtx *s_ctx;
static PetscReal s_K[2][2] = {{0,1},{-1,0}};

enum field_idx {NDEN,OMEGA,PSI,PHI,JZ,NUM_FIELDS};
enum auxfield_idx {LNNDEN0,OMEGA0,PSI0,NUM_AUX_FIELDS};

/*
*/
static void g0_dt(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[])
{
  g0[0] = u_tShift;
}

/*
*/
static void g0_1(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[])
{
  g0[0] = 1;
}

/* 'right' Poisson bracket -< . , phi0>, live var is left, held var is right */
static void g1_nphi_right(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                          const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                          const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                          PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pphiDer = &u_x[uOff_x[PHI]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[i] += -s_K[i][j]*pphiDer[j];
}
/* 'left' Poisson bracket -beta< j0_z , .>, live var is right, held var is left */
static void g1_nbetaj_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                           const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                           const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                           PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *jzDer   = &u_x[uOff_x[JZ]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += -s_ctx->beta*jzDer[i]*s_K[i][j];
}
/* 'left' Poisson bracket - < ln(n0) , . > - < n, .>, live var is right, held var is left */
static void g1_nlnn0_nn_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                             const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                             const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                             PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pnDer   = &u_x[uOff_x[NDEN]];
  const PetscScalar *logRefDenDer = &a_x[aOff_x[LNNDEN0]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += -(logRefDenDer[i] + pnDer[i])*s_K[i][j];
}

static void g1_nbetapsi_right(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                              const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                              const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                              PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *psiDer   = &u_x[uOff_x[PSI]];
  const PetscScalar *RefPsiDer = &a_x[aOff_x[PSI0]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[i] += -s_K[i][j]*s_ctx->beta*(psiDer[j]+RefPsiDer[j]);
}

/* 'left' Poisson bracket - < Omega , . >, live var is right, held var is left */
static void g1_nomega_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
			   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
			   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
			   PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pOmegaDer   = &u_x[uOff_x[OMEGA]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += -pOmegaDer[i]*s_K[i][j];
}

/* 'left' Poisson bracket < psi , . > + < psi0 , . >, live var is right, held var is left */
static void g1_psi0_psi_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                             const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                             const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                             PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pPsiDer   = &u_x[uOff_x[PSI]];
  const PetscScalar *RefPsiDer = &a_x[aOff_x[PSI0]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += (pPsiDer[i]+RefPsiDer[i])*s_K[i][j];
}

/* 'left' Poisson bracket -< psi , . > + < psi0 , . >, live var is right, held var is left */
static void g1_npsi0_npsi_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                             const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                             const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                             PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pPsiDer   = &u_x[uOff_x[PSI]];
  const PetscScalar *RefPsiDer = &a_x[aOff_x[PSI0]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += -(pPsiDer[i]+RefPsiDer[i])*s_K[i][j];
}

/* 'left' Poisson bracket < ln(n0) , . >, live var is right, held var is left */
/* 'right' Poisson bracket -< . , phi> + < . , n>, live var is left, held var is right */
static void g1_lnn0_left_nphi_n_right(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                             const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                             const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                             PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pnDer        = &u_x[uOff_x[NDEN]];
  const PetscScalar *pphiDer      = &u_x[uOff_x[PHI]];
  const PetscScalar *logRefDenDer = &a_x[aOff_x[LNNDEN0]];
  /* left */
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += logRefDenDer[i]*s_K[i][j];
  /* right */
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[i] += s_K[i][j]*(pnDer[j]-pphiDer[j]);
}

static void g3_nmu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) {
    g3[d*dim+d] = -s_ctx->mu;
  }
}

static void g3_neta_beta(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) {
    g3[d*dim+d] = -s_ctx->eta/s_ctx->beta;
  }
}

static void g3_1(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) {
    g3[d*dim+d] = 1;
  }
}

static void g3_n1(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) {
    g3[d*dim+d] = -1;
  }
}


/* residual point methods */
static PetscScalar poissonBracket(PetscInt dim, const PetscScalar df[], const PetscScalar dg[])
{
  PetscScalar ret = df[0]*dg[1] - df[1]*dg[0];
  /* if (dim==3) { */
  /*   ret += df[1]*dg[2] - df[2]*dg[1]; */
  /*   ret += df[2]*dg[0] - df[0]*dg[2]; */
  /* } */
  return ret;
}

static void f0_n(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  const PetscScalar *pnDer   = &u_x[uOff_x[NDEN]];
  const PetscScalar *ppsiDer = &u_x[uOff_x[PSI]];
  const PetscScalar *refPsiDer    = &a_x[aOff_x[PSI0]];
  const PetscScalar *pphiDer = &u_x[uOff_x[PHI]];
  const PetscScalar *jzDer   = &u_x[uOff_x[JZ]];
  const PetscScalar *logRefDenDer = &a_x[aOff_x[LNNDEN0]];
  PetscScalar       psiDer[3];
  PetscInt           d;
  for (d = 0; d < dim; ++d) {
    psiDer[d]    = refPsiDer[d] + ppsiDer[d];
  }
  f0[0] += -poissonBracket(dim,pnDer, pphiDer) - s_ctx->beta*poissonBracket(dim,jzDer, psiDer) - poissonBracket(dim,logRefDenDer, pphiDer);
  if (u_t) f0[0] += u_t[NDEN];
  /* PetscPrintf(PETSC_COMM_WORLD, "f0_n: %e: %e %e; %e %e; %e %e; %e %e; %e %e; u_t=%e\n",f0[0],pnDer[0],pnDer[1],ppsiDer[0],ppsiDer[1],pphiDer[0],pphiDer[1],jzDer[0],jzDer[1],logRefDenDer[0],logRefDenDer[1],u_t ? u_t[NDEN] : 0.0); */
}

static void f1_n(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *pnDer = &u_x[uOff_x[NDEN]];
  PetscInt           d;
  for (d = 0; d < 2; ++d) f1[d] = -s_ctx->mu*pnDer[d];
}

static void f0_Omega(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                     const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                     const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                     PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  const PetscScalar *pOmegaDer = &u_x[uOff_x[OMEGA]];
  const PetscScalar *ppsiDer   = &u_x[uOff_x[PSI]];
  const PetscScalar *refPsiDer    = &a_x[aOff_x[PSI0]];
  const PetscScalar *pphiDer   = &u_x[uOff_x[PHI]];
  const PetscScalar *jzDer     = &u_x[uOff_x[JZ]];
  PetscScalar       psiDer[3];
  PetscInt           d;
  for (d = 0; d < dim; ++d) {
    psiDer[d]    = refPsiDer[d] + ppsiDer[d];
  }
  f0[0] += - poissonBracket(dim,pOmegaDer, pphiDer) - s_ctx->beta*poissonBracket(dim,jzDer, psiDer);
  if (u_t) f0[0] += u_t[OMEGA];
}

static void f1_Omega(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                     const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                     const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                     PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *pOmegaDer = &u_x[uOff_x[OMEGA]];
  PetscInt           d;

  for (d = 0; d < dim; ++d) f1[d] = -s_ctx->mu*pOmegaDer[d];
}

static void f0_psi(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                   PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  const PetscScalar *pnDer        = &u_x[uOff_x[NDEN]];
  const PetscScalar *ppsiDer      = &u_x[uOff_x[PSI]];
  const PetscScalar *pphiDer      = &u_x[uOff_x[PHI]];
  const PetscScalar *refPsiDer    = &a_x[aOff_x[PSI0]];
  const PetscScalar *logRefDenDer = &a_x[aOff_x[LNNDEN0]];
  PetscScalar       psiDer[3];
  PetscScalar       phi_n_Der[3];
  PetscInt           d;
  for (d = 0; d < dim; ++d) {
    psiDer[d]    = refPsiDer[d] + ppsiDer[d];
    phi_n_Der[d] = pphiDer[d]   - pnDer[d];
  }
  f0[0] = - poissonBracket(dim,psiDer, phi_n_Der) + poissonBracket(dim,logRefDenDer, ppsiDer);
  if (u_t) f0[0] += u_t[PSI];
  //printf("ppsiDer = %20.15e %20.15e psi = %20.15e refPsiDer = %20.15e %20.15e refPsi = %20.15e \n",ppsiDer[0],ppsiDer[1],u[PSI],refPsiDer[0],refPsiDer[1],a[PSI]);
}

static void f1_psi(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                   PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *ppsi = &u_x[uOff_x[PSI]];
  PetscInt           d;

  for (d = 0; d < dim; ++d) f1[d] = -(s_ctx->eta/s_ctx->beta)*ppsi[d];
}

static void f0_phi(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                   PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  f0[0] = u[uOff[OMEGA]];
}

static void f1_phi(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                   PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *pphiDer = &u_x[uOff_x[PHI]];
  PetscInt           d;

  for (d = 0; d < dim; ++d) f1[d] = -pphiDer[d];
}

static void f0_jz(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  f0[0] = u[uOff[JZ]];
}

/* del^2 (psi + psi_0) */
static void f1_jz(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *ppsiDer   = &u_x[uOff_x[PSI]];
  const PetscScalar *refPsiDer = &a_x[aOff_x[PSI0]];
  PetscInt           d;
  for (d = 0; d < dim; ++d) f1[d] = ppsiDer[d] + refPsiDer[d];
}

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscBool      flg;
  PetscErrorCode ierr;
  PetscInt       ii, bd;
  PetscReal      a,b,buffer[3*MHD_MAX_SPECTRAL];
  PetscFunctionBeginUser;
  options->debug               = 1;
  options->print_fft           = 0;
  options->dim                 = 2;
  options->filename[0]         = '\0';
  options->cell_simplex        = PETSC_FALSE;
  options->implicit            = PETSC_TRUE;
  options->refine              = 2;
  options->domain_lo[0]  = 0.0;
  options->domain_lo[1]  = 0.0;
  options->domain_lo[2]  = 0.0;
  options->domain_hi[0]  = 2.0;
  options->domain_hi[1]  = 2*PETSC_PI;
  options->domain_hi[2]  = 2.0;
  options->periodicity[0]= DM_BOUNDARY_NONE;
  options->periodicity[1]= DM_BOUNDARY_NONE;
  options->periodicity[2]= DM_BOUNDARY_NONE;
  options->mu   = 0.1;
  options->eta  = 0.001;
  options->beta = 0.1;
  options->ves = 0.005;
  options->Jop = 0.0;
  options->m = 1;
  options->eps = 1.e-6;
  options->fft_num_lines = 0; /* this is set with fft_line_coord */
  /* options->dt = 1; */
  /* options->dt_reduction_factor = 4; */
  options->iteration_dt_trigger = 4;

  ierr = PetscOptionsBegin(comm, "", "Poisson Problem Options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-debug", "The debugging level", "mhd.c", options->debug, &options->debug, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-print_fft", "Print Matlab spectra and solution", "mhd.c", options->print_fft, &options->print_fft, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "mhd.c", options->dim, &options->dim, NULL);CHKERRQ(ierr);
  for (ii = 0; ii < options->dim; ++ii) options->cells[ii] = 2;
  ierr = PetscOptionsInt("-dm_refine", "Hack to get refinement level for cylinder", "mhd.c", options->refine, &options->refine, NULL);CHKERRQ(ierr);
  /* ierr = PetscOptionsReal("-ts_dt", "initial time step", "mhd.c", options->dt, &options->dt, NULL);CHKERRQ(ierr); */
  /* ierr = PetscOptionsReal("-dt_reduction_factor", "Time step reduction factor", "mhd.c", options->dt_reduction_factor, &options->dt_reduction_factor, NULL);CHKERRQ(ierr); */
  ierr = PetscOptionsInt("-iteration_dt_trigger", "Number of non-linear iterations to trigger time step reduction", "mhd.c", options->iteration_dt_trigger, &options->iteration_dt_trigger, NULL);CHKERRQ(ierr);

  ierr = PetscOptionsReal("-mu", "mu", "mhd.c", options->mu, &options->mu, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-eta", "eta", "mhd.c", options->eta, &options->eta, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-beta", "beta", "mhd.c", options->beta, &options->beta, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ves", "ves", "mhd.c", options->ves, &options->ves, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-Jop", "Jop", "mhd.c", options->Jop, &options->Jop, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-m", "m", "mhd.c", options->m, &options->m, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-eps", "eps", "mhd.c", options->eps, &options->eps, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-f", "Exodus.II filename to read", "mhd.c", options->filename, options->filename, sizeof(options->filename), &flg);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-cell_simplex", "Simplicial (true) or tensor (false) mesh", "mhd.c", options->cell_simplex, &options->cell_simplex, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-implicit", "Use implicit time integrator", "mhd.c", options->implicit, &options->implicit, NULL);CHKERRQ(ierr);
  ii = options->dim;
  ierr = PetscOptionsRealArray("-domain_hi", "Domain size", "mhd.c", options->domain_hi, &ii, NULL);CHKERRQ(ierr);
  ii = options->dim;
  ierr = PetscOptionsRealArray("-domain_lo", "Domain size", "mhd.c", options->domain_lo, &ii, NULL);CHKERRQ(ierr);
  ii = options->dim;
  bd = options->periodicity[0];
  ierr = PetscOptionsEList("-x_periodicity", "The x-boundary periodicity", "mhd.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[0]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[0] = (DMBoundaryType) bd;
  bd = options->periodicity[1];  /* this hardwires the size of DMBoundaryTypes at 5, yuck */
  ierr = PetscOptionsEList("-y_periodicity", "The y-boundary periodicity", "mhd.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[1]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[1] = (DMBoundaryType) bd;
  bd = options->periodicity[2];
  ierr = PetscOptionsEList("-z_periodicity", "The z-boundary periodicity", "mhd.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[2]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[2] = (DMBoundaryType) bd;
  ii = options->dim;
  ierr = PetscOptionsIntArray("-cells", "Number of cells in each dimension", "mhd.c", options->cells, &ii, NULL);CHKERRQ(ierr);
  ii = MHD_MAX_SPECTRAL;
  for (bd = 1; bd < 3 ; bd++) options->fft_line_dir[bd] = 1; /* default in y dir */
  ierr = PetscOptionsIntArray("-fft_line_dir", "2D direction of lines for FFT, eg, -fft_line_dir 1 uses a line in the y direction: x = q (with -fft_line_coord q)", "mhd.c", options->fft_line_dir, &ii, NULL);CHKERRQ(ierr);
  options->fft_component = 0; /* density */
  ierr = PetscOptionsInt("-fft_component", "component of FFT, one based index", "mhd.c", options->fft_component, &options->fft_component, &flg);CHKERRQ(ierr);
  if (flg) options->fft_component--; /* make zero based */
  /* set default line coordinate at 1/2 the domain */
  for (ii = 0; ii < options->dim; ++ii)
    for (bd = 0; bd < MHD_MAX_SPECTRAL ; bd++)
      options->fft_line_coord[bd][ii] = options->domain_lo[ii] + (options->domain_hi[ii] - options->domain_lo[ii])/2.;
  ii = (options->dim-1)*MHD_MAX_SPECTRAL;
  ierr = PetscOptionsRealArray("-fft_line_coord", "2D coordinate in FFT line direction (-fft_line_dir)", "mhd.c", buffer, &ii, NULL);CHKERRQ(ierr);
  options->fft_num_lines = ii;
  if (options->fft_num_lines > 1 && ii < options->dim-1) SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"Need to set line location with more than one direction = %D",options->fft_num_lines);
  if (options->fft_num_lines > 1 && ii != options->fft_num_lines*(options->dim-1)) SETERRQ3(comm,PETSC_ERR_ARG_WRONG,"Need to set line location with more than one direction = %D, with %D values, not %D",options->fft_num_lines,options->fft_num_lines*(options->dim-1),ii);
  for (bd = 0; bd < options->fft_num_lines ; bd++) {
    int kk;
    for (kk=0;kk<options->dim-1;kk++) options->fft_line_coord[bd][kk] = buffer[(options->dim-1)*bd + kk];
  }
  ierr = PetscOptionsEnd();
  a = (options->domain_hi[0]-options->domain_lo[0])/2.0; /* x is symmetric around 0, while y is periodic.  this leads to the 2 in a being convenient notation. */
  b = (options->domain_hi[1]-options->domain_lo[1]);
  for (ii = 0; ii < options->dim; ++ii) {
    if (options->domain_hi[ii] <= options->domain_lo[ii]) SETERRQ3(comm,PETSC_ERR_ARG_WRONG,"Domain %D lo=%g hi=%g",ii,options->domain_lo[ii],options->domain_hi[ii]);
  }
  options->ke = PetscSqrtScalar(options->Jop);
  if (options->Jop==0.0) {
    options->Jo = 1.0/PetscPowScalar(a,2);
  } else {
    options->Jo = options->Jop*PetscCosReal(options->ke*a)/(1.0-PetscCosReal(options->ke*a));
  }
  options->ky = 2*PETSC_PI*options->m/b;
  if (PetscPowScalar(options->ky,2)<options->Jop) {
    options->kx = PetscSqrtScalar(options->Jop-PetscPowScalar(options->ky,2));
    options->DeltaPrime = -2.0*options->kx*a*PetscCosReal(options->kx*a)/PetscSinReal(options->kx*a);
  } else if (PetscPowScalar(options->ky,2)>options->Jop) {
    options->kx = PetscSqrtScalar(PetscPowScalar(options->ky,2)-options->Jop);
    options->DeltaPrime = -2.0*options->kx*a*PetscCoshReal(options->kx*a)/PetscSinhReal(options->kx*a);
  } else { //they're equal (or there's a NaN), lim(x*cot(x))_x->0=1
    options->kx = 0;
    options->DeltaPrime = -2.0;
  }
  ierr = PetscPrintf(comm, "Jop=%g\n",options->Jop);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "m=%g\n",options->m);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "a=%g\n",a);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "b=%g\n",b);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "ky=%g\n",options->ky);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "kx=%g\n",options->kx);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "DeltaPrime=%g\n",options->DeltaPrime);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "eta=%g\n",options->eta);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "beta=%g\n",options->beta);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "mu=%g\n",options->mu);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "ves=%g\n",options->ves);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* #undef __FUNCT__ */
/* #define __FUNCT__ "f_n" */
/* static void f_n(PetscInt dim, PetscInt Nf, PetscInt NfAux, */
/*                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[], */
/*                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[], */
/*                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar *f0) */
/* { */
/*   const PetscScalar *pn = &u[uOff[NDEN]]; */
/*   *f0 = *pn; */
/* } */

#undef __FUNCT__
#define __FUNCT__ "PreStep"
static PetscErrorCode PreStep(TS ts)
{
  PetscErrorCode    ierr;
  DM                dm;
  AppCtx            *ctx;
  PetscInt          num;
  Vec               X;
  PetscReal         val;
  PetscFunctionBegin;
  ierr = TSGetApplicationContext(ts, &ctx);CHKERRQ(ierr); assert(ctx);
  if (ctx->debug<1) PetscFunctionReturn(0);
  ierr = TSGetSolution(ts, &X);CHKERRQ(ierr);
  ierr = VecGetDM(X, &dm);CHKERRQ(ierr);
  ierr = DMGetOutputSequenceNumber(dm, &num, &val);CHKERRQ(ierr);
  if (num < 0) {ierr = DMSetOutputSequenceNumber(dm, 0, 0.0);CHKERRQ(ierr);}
  if (num < 1) { /* initial state only */
    ierr = PetscObjectSetName((PetscObject) X, "u");CHKERRQ(ierr);
    ierr = VecViewFromOptions(X, NULL, "-vec_view");CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}
static PetscErrorCode ComputeSpectral(DM dm, Vec u, PetscInt tsnum, AppCtx *ctx)
{
  MPI_Comm           comm;
  PetscSection       coordSection, section;
  Vec                coordinates, uloc;
  const PetscScalar *coords, *array;
  PetscInt           p, dim = ctx->dim;
  PetscMPIInt        size, rank;
  PetscErrorCode     ierr;

  PetscFunctionBeginUser;
  if (ctx->print_fft<1) PetscFunctionReturn(0);
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (ctx->dim != 2) SETERRQ(comm,PETSC_ERR_ARG_WRONG,"3D not implemented");
  ierr = MPI_Comm_size(comm, &size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm, &uloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm, u, INSERT_VALUES, uloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm, u, INSERT_VALUES, uloc);CHKERRQ(ierr);
  ierr = DMPlexInsertBoundaryValues(dm, PETSC_TRUE, uloc, 0.0, NULL, NULL, NULL);CHKERRQ(ierr);
  ierr = DMGetDefaultSection(dm, &section);CHKERRQ(ierr);
  ierr = VecGetArrayRead(uloc, &array);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMGetCoordinateSection(dm, &coordSection);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coordinates, &coords);CHKERRQ(ierr);
  for (p = 0; p < ctx->fft_num_lines; ++p) {
    DMLabel         label;
    char            name[PETSC_MAX_PATH_LEN],nameu[PETSC_MAX_PATH_LEN],namefft[PETSC_MAX_PATH_LEN];
    Mat             F;
    Vec             x, y;
    IS              stratum;
    PetscReal      *ray, *gray;
    PetscScalar    *rvals, *svals, *gsvals;
    PetscInt       *perm, *nperm;
    PetscInt        n, N, Nsq, i, j, off, offu;
    const PetscInt *points;

    ierr = PetscSNPrintf(nameu, PETSC_MAX_PATH_LEN, "u_%D", p);CHKERRQ(ierr);
    ierr = PetscSNPrintf(namefft, PETSC_MAX_PATH_LEN, "fft_%D", p);CHKERRQ(ierr);
    ierr = PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "line_%D", p);CHKERRQ(ierr);
    ierr = DMGetLabel(dm, name, &label);CHKERRQ(ierr);
    ierr = DMLabelGetStratumIS(label, 1, &stratum);CHKERRQ(ierr);
    if (stratum) { /* empty list */
      ierr = ISGetLocalSize(stratum, &n);CHKERRQ(ierr);
      ierr = ISGetIndices(stratum, &points);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD, "\t[%D]ComputeSpectral: %D line for label '%s', n = %D\n",rank,p,name,n);
    } else {
      PetscPrintf(PETSC_COMM_WORLD, "[%D]ComputeSpectral: Warning: %D line is empty for label '%s'\n",rank,p,name);
      n = 0;
      points = NULL;
    }
    ierr = PetscMalloc2(n*(dim-1), &ray, n*(dim-1), &svals);CHKERRQ(ierr);
    for (i = 0; i < n; ++i) {
      ierr = PetscSectionGetOffset(coordSection, points[i], &off);CHKERRQ(ierr);
      ierr = PetscSectionGetOffset(section, points[i], &offu);CHKERRQ(ierr);
      ray[i*(dim-1)]   = PetscRealPart(coords[off + (ctx->fft_line_dir[p])]); /* In 2D the coordinate is just the direction */
      if (dim==3) ray[i*(dim-1)+1] = 0; /* todo */
      svals[i] = array[offu + ctx->fft_component];
      /* PetscPrintf(PETSC_COMM_WORLD, "\t[%D]ComputeSpectral: ray = %g line dir = %D\n",rank,ray[i],ctx->fft_line_dir[p]); */
    }
    /* Gather the ray data to proc 0 */
    if (size > 1) {
      int *cnt, *displs;
      ierr = PetscCalloc2(size, &cnt, size, &displs);CHKERRQ(ierr);
      ierr = MPI_Gather(&n, 1, MPIU_INT, cnt, 1, MPIU_INT, 0, comm);CHKERRQ(ierr);
      for (j = 1; j < size; ++j) displs[j] = displs[j-1] + cnt[j-1];
      N = displs[size-1] + cnt[size-1];
      ierr = PetscMalloc2(N, &gray, N, &gsvals);CHKERRQ(ierr);
      ierr = MPI_Gatherv(ray, n, MPIU_REAL, gray, cnt, displs, MPIU_REAL, 0, comm);CHKERRQ(ierr);
      ierr = MPI_Gatherv(svals, n, MPIU_SCALAR, gsvals, cnt, displs, MPIU_SCALAR, 0, comm);CHKERRQ(ierr);
      ierr = PetscFree2(cnt, displs);CHKERRQ(ierr);
    } else {
      N      = n;
      gray   = ray;
      gsvals = svals;
    }
    if (!rank && N) {
      char str[PETSC_MAX_PATH_LEN];
      /* Sort point along ray */
      ierr = PetscMalloc2(N, &perm, N, &nperm);CHKERRQ(ierr);
      for (i = 0; i < N; ++i) { perm[i] = i;}
      ierr = PetscSortRealWithPermutation(N, gray, perm);CHKERRQ(ierr);
      /* Count duplicates and squish mapping */
      nperm[0] = perm[0];
      for (i = 1, j = 1; i < N; ++i) {
        if (PetscAbsReal(gray[perm[i]] - gray[perm[i-1]]) > PETSC_SMALL) {
	  nperm[j++] = perm[i];
	}
      }
      Nsq = j; /* squeezed N */
      PetscPrintf(PETSC_COMM_WORLD, "[%D]ComputeSpectral: %D/%D) MatCreateFFT: N (local) =%D\n",0,p+1,ctx->fft_num_lines,Nsq);
      ierr = MatCreateFFT(PETSC_COMM_SELF, 1, &Nsq, MATFFTW, &F);CHKERRQ(ierr);
      ierr = MatCreateVecs(F, &x, &y);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) y, namefft);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) x, nameu);CHKERRQ(ierr);
      ierr = VecGetArray(x, &rvals);CHKERRQ(ierr);
      for (i = 0, j = 0; i < N; ++i) {
	if (i > 0 && PetscAbsReal(gray[perm[i]] - gray[perm[i-1]]) <= PETSC_SMALL) continue;
        rvals[j] = gsvals[nperm[j]];
	j++;
      }
      assert(Nsq == j);
      ierr = PetscFree2(perm, nperm);CHKERRQ(ierr);
      if (size > 1) {ierr = PetscFree2(gray, gsvals);CHKERRQ(ierr);}
      ierr = VecRestoreArray(x, &rvals);CHKERRQ(ierr);
      /* Do FFT along the ray */
      ierr = MatMult(F, x, y);CHKERRQ(ierr);
      // ierr = VecChop(y, PETSC_SMALL);CHKERRQ(ierr);
      if (ctx->print_fft>1) { /* view u */
	/* hack to get the sequence number into the fine name */
	ierr = PetscSNPrintf(str, PETSC_MAX_PATH_LEN, ":u%06D.m:ascii_matlab", tsnum);CHKERRQ(ierr);
	ierr = PetscOptionsClearValue(NULL,"-real_view");CHKERRQ(ierr);
	ierr = PetscOptionsSetValue(NULL,"-real_view",str);CHKERRQ(ierr);
	ierr = VecViewFromOptions(x, NULL, "-real_view");CHKERRQ(ierr);
      }
      /* hack to get the sequence number into the fine name */
      ierr = PetscSNPrintf(str, PETSC_MAX_PATH_LEN, ":spectra%06D.m:ascii_matlab", tsnum);CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-fft_view");CHKERRQ(ierr);
      ierr = PetscOptionsSetValue(NULL,"-fft_view",str);CHKERRQ(ierr);
      ierr = VecViewFromOptions(y, NULL, "-fft_view");CHKERRQ(ierr);
      ierr = VecDestroy(&x);CHKERRQ(ierr);
      ierr = VecDestroy(&y);CHKERRQ(ierr);
      ierr = MatDestroy(&F);CHKERRQ(ierr);
    }
    if (stratum) {
      ierr = ISRestoreIndices(stratum, &points);CHKERRQ(ierr);
      ierr = ISDestroy(&stratum);CHKERRQ(ierr);
    }
    ierr = PetscFree2(ray, svals);CHKERRQ(ierr);
  }
  ierr = VecRestoreArrayRead(coordinates, &coords);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(uloc, &array);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm, &uloc);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PostStep"
static PetscErrorCode PostStep(TS ts)
{
  PetscErrorCode      ierr;
  DM                  dm;
  AppCtx              *ctx;
  PetscInt            num,stepi;
  Vec                 X;
  PetscReal           val;
  SNES                snes;
  SNESConvergedReason reason;
  PetscFunctionBegin;
  ierr = TSGetApplicationContext(ts, &ctx);CHKERRQ(ierr); assert(ctx);
  if (ctx->debug<1) PetscFunctionReturn(0);
  ierr = TSGetSNES(ts,&snes);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes,&reason);CHKERRQ(ierr);
  if (reason<0) {
    PetscPrintf(PetscObjectComm((PetscObject)ts), "\t\t% PostStep: SNES diverged with reason %D.\n",reason);CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }
  ierr = TSGetSolution(ts, &X);CHKERRQ(ierr);
  ierr = VecGetDM(X, &dm);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) X, "u");CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &stepi);CHKERRQ(ierr);
  /* if (stepi%ctx->debug==0) { */
    DM                  plex;
    ierr = DMGetOutputSequenceNumber(dm, &num, &val);CHKERRQ(ierr);
    /* PetscPrintf(PetscObjectComm((PetscObject)ts), "\t\t% PostStep: num %D.\n",num+1);CHKERRQ(ierr);  */
    ierr = DMSetOutputSequenceNumber(dm, ++num, val);CHKERRQ(ierr); /* need this to keep ahead of initial state */
    ierr = DMConvert(dm, DMPLEX, &plex);CHKERRQ(ierr);
    ierr = VecViewFromOptions(X, NULL, "-vec_view");CHKERRQ(ierr);
    ierr = ComputeSpectral(plex, X, num, ctx);CHKERRQ(ierr);
    /* clean up */
    ierr = DMDestroy(&plex);CHKERRQ(ierr);
  /* } */
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateBCLabel(DM dm, const char name[])
{
  DMLabel        label;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = DMCreateLabel(dm, name);CHKERRQ(ierr);
  ierr = DMGetLabel(dm, name, &label);CHKERRQ(ierr);
  ierr = DMPlexMarkBoundaryFaces(dm, PETSC_DETERMINE, label);CHKERRQ(ierr);
  ierr = DMPlexLabelComplete(dm, label);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateLineLabels(DM dm, char name[][PETSC_MAX_PATH_LEN], AppCtx *ctx )
{
  DMLabel        label[MHD_MAX_SPECTRAL];
  PetscErrorCode ierr;
  PetscInt       vStart, vEnd, v, z;
  PetscSection   coordSection;
  Vec            coordinates;
  PetscScalar    *array;
  PetscFunctionBeginUser;
  ierr = DMPlexGetDepthStratum(dm, 0, &vStart, &vEnd);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMGetCoordinateSection(dm, &coordSection);CHKERRQ(ierr);
  ierr = VecGetArray(coordinates, &array);CHKERRQ(ierr);
  /* create labels */
  for (z = 0; z < ctx->fft_num_lines; ++z) {
    ierr = PetscSNPrintf(name[z], PETSC_MAX_PATH_LEN, "line_%D", z);CHKERRQ(ierr);
    ierr = DMCreateLabel(dm, name[z]);CHKERRQ(ierr);
    ierr = DMGetLabel(dm, name[z], &label[z]);CHKERRQ(ierr);
  }
  /* set labels at z coords */
  for (v = vStart; v < vEnd; ++v) {
    PetscInt       off;
    ierr = PetscSectionGetOffset(coordSection, v, &off);CHKERRQ(ierr);
    for (z = 0 ; z < ctx->fft_num_lines; ++z) {
      if (ctx->dim == 3) {
	/* int have=0,idx=0,d; */
	/* /\* in 3D we need to look for the x,y point (z line) *\/ */
	/* for(d=0;d<3;d++) { */
	/*   if (d != ctx->fft_line_dir[z]) { */
	/*     PetscReal t = PetscRealPart(array[off + d]), x1 = ctx->fft_line_coord[z][idx++]; */
	/*     if (PetscAbsReal(x1 - t) <= PETSC_SMALL*t) have++; */
	/*   } */
	/* } */
	/* if (have==2) { */
	/*   ierr = DMLabelSetValue(label[z], v, 1);CHKERRQ(ierr); */
	/* } */
      } else {
	int d = (ctx->fft_line_dir[z]+1)%2;
	PetscReal t = PetscRealPart(array[off + d]), x1 = ctx->fft_line_coord[z][0];
	if (PetscAbsReal(x1 - t) <= PETSC_SMALL*t) {
	  ierr = DMLabelSetValue(label[z], v, 1);CHKERRQ(ierr);
	  /* static PetscMPIInt rank, cc=0;  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr); */
	  /* PetscPrintf(PETSC_COMM_SELF, "\t\t[%D]CreateLineLabels: %D) line %D, vertex %D, fft_line_coord=%e dir=%D x=%e %e\n",rank,++cc,z,v,x1,d,t,PetscRealPart(array[off + (d+1)%2])); */
	}
      }
    }
  }
  /* finish labels */
  for (z = 0; z < ctx->fft_num_lines; ++z) {
    ierr = DMPlexLabelComplete(dm, label[z]);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(coordinates, &array);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateMesh(MPI_Comm comm, AppCtx *ctx, DM *dm)
{
  PetscInt       dim      = ctx->dim;
  const char    *filename = ctx->filename;
  size_t         len;
  PetscMPIInt    numProcs;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = MPI_Comm_size(comm, &numProcs);CHKERRQ(ierr);
  ierr = PetscStrlen(filename, &len);CHKERRQ(ierr);
  if (len) {
    ierr = DMPlexCreateFromFile(comm, filename, PETSC_TRUE, dm);CHKERRQ(ierr);
  } else {
    PetscInt d,i;
    /* create DM */
    if (ctx->cell_simplex && dim == 3) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot mesh a cylinder with simplices");
    if (dim==2) {
      PetscInt npi, totCells = 1, nrefine = 0;
      char     str[8];
      if (ctx->cell_simplex) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot mesh 2D with simplices");
      npi = PetscMax((PetscInt) (PetscPowReal(numProcs, 1.0/dim) + 0.001), 1);
      for (d = 0; d < dim; ++d) {
        if (ctx->periodicity[d]==DM_BOUNDARY_PERIODIC && ctx->cells[d]*npi < 3) {
	  for (i = 0; i < dim; ++i) ctx->cells[i] *= 2;
	  nrefine++;
	}
	while (ctx->cells[d] < npi) {
	  for (i = 0; i < dim; ++i) ctx->cells[i] *= 2;
	  nrefine++;
	}
      }
      for (d = 0; d < dim; ++d) totCells *= ctx->cells[d];
      if (totCells < numProcs) SETERRQ5(comm,PETSC_ERR_ARG_WRONG,"Total cells %D less than number of processes %D, %D x %D cell grid, npi: %D", totCells, numProcs,ctx->cells[0],ctx->cells[1], npi);
      ierr = PetscPrintf(comm, "DMPlexCreateBoxMesh with %D total cells, %D x %D cell grid, with %D refinements\n",totCells,ctx->cells[0],ctx->cells[1],ctx->refine-nrefine);CHKERRQ(ierr);
      ierr = PetscSNPrintf(str, 8, "%D", ctx->refine - nrefine);CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-dm_refine");CHKERRQ(ierr);
      ierr = PetscOptionsSetValue(NULL,"-dm_refine",str);CHKERRQ(ierr);
      ierr = DMPlexCreateBoxMesh(comm, dim, PETSC_FALSE, ctx->cells, ctx->domain_lo, ctx->domain_hi, ctx->periodicity, PETSC_TRUE, dm);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) *dm, "Box Mesh");CHKERRQ(ierr);
    } else {
      if (ctx->periodicity[0]==DM_BOUNDARY_PERIODIC || ctx->periodicity[1]==DM_BOUNDARY_PERIODIC) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot do periodic in x or y in a cylinder");
      /* we stole dm_refine so clear it */
      ierr = PetscOptionsClearValue(NULL,"-dm_refine");CHKERRQ(ierr);
      ierr = DMPlexCreateHexCylinderMesh(comm, ctx->refine, ctx->periodicity[2], dm);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) *dm, "Cylinder Mesh");CHKERRQ(ierr);
    }
  }
  {
    DM               distributedMesh = NULL;
    PetscPartitioner part;
    ierr = DMPlexGetPartitioner(*dm,&part);CHKERRQ(ierr);
    ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
    /* Distribute mesh over processes */
    ierr = DMPlexDistribute(*dm, 0, NULL, &distributedMesh);CHKERRQ(ierr);
    if (distributedMesh) {
      ierr = DMDestroy(dm);CHKERRQ(ierr);
      *dm  = distributedMesh;
    }
  }
  {
    char      convType[256];
    PetscBool flg;
    ierr = PetscOptionsBegin(comm, "", "Mesh conversion options", "DMPLEX");CHKERRQ(ierr);
    ierr = PetscOptionsFList("-dm_plex_convert_type","Convert DMPlex to another format","mhd",DMList,DMPLEX,convType,256,&flg);CHKERRQ(ierr);
    ierr = PetscOptionsEnd();
    if (flg) {
      DM dmConv;
      ierr = DMConvert(*dm,convType,&dmConv);CHKERRQ(ierr);
      if (dmConv) {
        ierr = DMDestroy(dm);CHKERRQ(ierr);
        *dm  = dmConv;
      }
    }
  }
  ierr = DMLocalizeCoordinates(*dm);CHKERRQ(ierr); /* needed for periodic */
  ierr = DMSetFromOptions(*dm);CHKERRQ(ierr);
  {
    PetscBool hasLabel;
    ierr = DMHasLabel(*dm, "marker", &hasLabel);CHKERRQ(ierr);
    if (!hasLabel) {ierr = CreateBCLabel(*dm, "marker");CHKERRQ(ierr);}
  }
  /* Create Label on planes for FFTs */
  {
    DMLabel        label;
    char           name[MHD_MAX_SPECTRAL][PETSC_MAX_PATH_LEN];
    int            z;
    ierr = CreateLineLabels(*dm, name, ctx);CHKERRQ(ierr);
    for (z = 0; z < ctx->fft_num_lines; ++z) {
      ierr = DMGetLabel(*dm, name[z], &label);CHKERRQ(ierr);
    }
  }

  ierr = DMViewFromOptions(*dm, NULL, "-dm_view");CHKERRQ(ierr);

  if (/* user->viewHierarchy */ 0 ) {
    DM       cdm = *dm;
    PetscInt i   = 0;
    char     buf[256];
    
    while (cdm) {
      ierr = DMSetUp(cdm);CHKERRQ(ierr);
      ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
      ++i;
    }
    cdm = *dm;
    while (cdm) {
      PetscViewer       viewer;
      PetscBool   isHDF5, isVTK;
      --i;
      ierr = PetscViewerCreate(comm,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERHDF5);CHKERRQ(ierr);
      ierr = PetscViewerSetOptionsPrefix(viewer,"hierarchy_");CHKERRQ(ierr);
      ierr = PetscViewerSetFromOptions(viewer);CHKERRQ(ierr);
      ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERHDF5,&isHDF5);CHKERRQ(ierr);
      ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERVTK,&isVTK);CHKERRQ(ierr);
      if (isHDF5) {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d.h5", i);CHKERRQ(ierr);
      }
      else if (isVTK) {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d.vtu", i);CHKERRQ(ierr);
        ierr = PetscViewerPushFormat(viewer,PETSC_VIEWER_VTK_VTU);CHKERRQ(ierr);
      }
      else {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d", i);CHKERRQ(ierr);
      }
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,buf);CHKERRQ(ierr);
      ierr = DMView(cdm, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode log_n_0(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  AppCtx *lctx = (AppCtx*)ctx;
  assert(ctx);
  u[0] = lctx->ves*(1.0+x[0]/(lctx->domain_hi[0]-lctx->domain_lo[0]));
  return 0;
}

static PetscErrorCode Omega_0(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode psi_0(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *actx)
{
  AppCtx    *ctx = (AppCtx*)actx;
  PetscReal a,b;
  a = (ctx->domain_hi[0]-ctx->domain_lo[0])/2.0;
  b = (ctx->domain_hi[1]-ctx->domain_lo[1])/2.0;
  assert(ctx);
  /* This sets up a symmetrix By flux aroound the mid point in x, which represents a current density flux along z.  The stability
     is analytically known and reported in ProcessOptions. */
  if (ctx->ke!=0.0) {
    u[0] = (PetscCosReal(ctx->ke*(x[0]-a))-PetscCosReal(ctx->ke*a))/(1.0-PetscCosReal(ctx->ke*a));
  } else {
    u[0] = 1.0-PetscPowScalar((x[0]-a)/a,2);
  }
  return 0;
}

static PetscErrorCode initialSolution_n(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode initialSolution_Omega(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode initialSolution_psi(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *a_ctx)
{
  AppCtx    *ctx = (AppCtx*)a_ctx;
  PetscReal *lo = ctx->domain_lo, *hi = ctx->domain_hi, r;
  unsigned  i,hashCode;
  if (x[0] == ctx->domain_lo[0] || x[0] == ctx->domain_hi[0]) {
    r = 0;
  } else {
    for (i=0;i<dim;i++) {
      unsigned xi = (unsigned)((x[i]-lo[i])/(hi[i]-lo[i])*(PetscReal)RAND_MAX);
      if (i==0) hashCode = xi*73856093;
      else if (i==1) hashCode = hashCode ^ xi*19349663;
      else hashCode = hashCode ^ xi*83492791;
    }
    srand(hashCode);
    r = ctx->eps*(PetscReal) (rand()) / (PetscReal) (RAND_MAX);
  }
  u[0] = r;
  return 0;
}

static PetscErrorCode initialSolution_phi(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode initialSolution_jz(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode SetupProblem(PetscDS prob, AppCtx *ctx)
{
  const PetscInt id = 1;
  PetscErrorCode ierr, f;

  PetscFunctionBeginUser;
  if (ctx->implicit) {
    ierr = PetscDSSetJacobian(prob, NDEN, NDEN, g0_dt, g1_nphi_right,  NULL, g3_nmu);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, NDEN, PSI, NULL,  g1_nbetaj_left, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, NDEN, PHI, NULL,  g1_nlnn0_nn_left, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, NDEN, JZ, NULL,  g1_nbetapsi_right, NULL, NULL);CHKERRQ(ierr);

    ierr = PetscDSSetJacobian(prob, OMEGA, OMEGA, g0_dt, g1_nphi_right,  NULL, g3_nmu);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, OMEGA, PSI, NULL,    g1_nbetaj_left, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, OMEGA, PHI, NULL,    g1_nomega_left, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, OMEGA, JZ, NULL,     g1_nbetapsi_right, NULL, NULL);CHKERRQ(ierr);

    ierr = PetscDSSetJacobian(prob, PSI, NDEN, NULL,  g1_psi0_psi_left, NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, PSI, PSI, g0_dt, g1_lnn0_left_nphi_n_right,  NULL, g3_neta_beta);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, PSI, PHI, NULL,  g1_npsi0_npsi_left,  NULL, NULL);CHKERRQ(ierr);

    ierr = PetscDSSetJacobian(prob, PHI, OMEGA, g0_1,  NULL,  NULL, NULL);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, PHI, PHI,   NULL,  NULL,  NULL, g3_n1);CHKERRQ(ierr);

    ierr = PetscDSSetJacobian(prob, JZ, PSI,  NULL,  NULL,  NULL, g3_1);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, JZ, JZ,   g0_1,  NULL,  NULL, NULL);CHKERRQ(ierr);
  }
  ierr = PetscDSSetResidual(prob, NDEN,  f0_n,     f1_n);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, OMEGA, f0_Omega, f1_Omega);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, PSI,   f0_psi,   f1_psi);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, PHI,   f0_phi,   f1_phi);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, JZ,    f0_jz,    f1_jz);CHKERRQ(ierr);

  ctx->initialFuncs[0] = initialSolution_n;
  ctx->initialFuncs[1] = initialSolution_Omega;
  ctx->initialFuncs[2] = initialSolution_psi;
  ctx->initialFuncs[3] = initialSolution_phi;
  ctx->initialFuncs[4] = initialSolution_jz;
  for (f = 0; f < MHD_NUM_COMP; ++f) {
    ierr = PetscDSSetImplicit( prob, f, ctx->implicit);CHKERRQ(ierr);
    ierr = PetscDSAddBoundary( prob, DM_BC_ESSENTIAL, "wall", "marker", f, 0, NULL, (void (*)()) ctx->initialFuncs[f], 1, &id, ctx);CHKERRQ(ierr);
  }
  ierr = PetscDSSetContext(prob, 0, ctx);CHKERRQ(ierr);
  ierr = PetscDSSetFromOptions(prob);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupEquilibriumFields(DM dm, DM dmAux, AppCtx *ctx)
{
  PetscErrorCode (*eqFuncs[3])(PetscInt, PetscReal, const PetscReal [], PetscInt, PetscScalar [], void *) = {log_n_0, Omega_0, psi_0};
  Vec            eq;
  PetscErrorCode ierr;
  AppCtx *ctxarr[3]={ctx,ctx,ctx}; //each variable could have a different context

  PetscFunctionBegin;
  ierr = DMCreateLocalVector(dmAux, &eq);CHKERRQ(ierr);
  ierr = DMProjectFunctionLocal(dmAux, 0.0, eqFuncs, (void **)ctxarr, INSERT_ALL_VALUES, eq);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) eq);CHKERRQ(ierr);
  if (ctx->debug > 2) {  /* plot reference functions */
    PetscViewer       viewer = NULL;
    PetscBool         isHDF5,isVTK;
    char              buf[256];
    Vec               global;
    ierr = DMCreateGlobalVector(dmAux,&global);CHKERRQ(ierr);
    ierr = VecSet(global,.0);CHKERRQ(ierr); /* BCs! */
    ierr = DMLocalToGlobalBegin(dmAux,eq,INSERT_VALUES,global);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(dmAux,eq,INSERT_VALUES,global);CHKERRQ(ierr);
    ierr = PetscViewerCreate(PetscObjectComm((PetscObject)dmAux),&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer,PETSCVIEWERHDF5);CHKERRQ(ierr);
    ierr = PetscViewerSetFromOptions(viewer);CHKERRQ(ierr);
    ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERHDF5,&isHDF5);CHKERRQ(ierr);
    ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERVTK,&isVTK);CHKERRQ(ierr);
    if (isHDF5) {
      ierr = PetscSNPrintf(buf, 256, "uEquilibrium-%dD.h5", ctx->dim);CHKERRQ(ierr);
    } else if (isVTK) {
      ierr = PetscSNPrintf(buf, 256, "uEquilibrium-%dD.vtu", ctx->dim);CHKERRQ(ierr);
      ierr = PetscViewerPushFormat(viewer,PETSC_VIEWER_VTK_VTU);CHKERRQ(ierr);
    }
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer,buf);CHKERRQ(ierr);
    if (isHDF5) {
      ierr = DMView(dmAux,viewer);CHKERRQ(ierr);
    }
    /* view equilibrium fields, this will overwrite fine grids with coarse grids! */
    ierr = PetscObjectSetName((PetscObject) global, "u0");CHKERRQ(ierr);
    ierr = VecView(global,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = VecDestroy(&global);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&eq);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupDiscretization(DM dm, AppCtx *ctx)
{
  DM              cdm = dm;
  const PetscInt  dim = ctx->dim;
  PetscQuadrature q;
  PetscFE         fe[MHD_NUM_COMP], feAux[3];
  PetscDS         prob, probAux;
  PetscInt        Nf = 5, NfAux = 3, f;
  PetscBool       cell_simplex = ctx->cell_simplex;
  PetscErrorCode  ierr;
  MPI_Comm        comm = PetscObjectComm((PetscObject) dm);
  
  PetscFunctionBeginUser;
  /* Create finite element */
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[0]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[0], "density");CHKERRQ(ierr);
  ierr = DMSetField(dm, 0, NULL, (PetscObject) fe[0]);CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[1]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[1], "vorticity");CHKERRQ(ierr);
  ierr = DMSetField(dm, 1, NULL, (PetscObject) fe[1]);CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[2]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[2], "flux");CHKERRQ(ierr);
  ierr = DMSetField(dm, 2, NULL, (PetscObject) fe[2]);CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[3]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[3], "potential");CHKERRQ(ierr);
  ierr = DMSetField(dm, 3, NULL, (PetscObject) fe[3]);CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[4]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[4], "current");CHKERRQ(ierr);
  ierr = DMSetField(dm, 4, NULL, (PetscObject) fe[4]);CHKERRQ(ierr);

  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &feAux[0]);CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe[0], &q);CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(feAux[0], q);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) feAux[0], "n_0");CHKERRQ(ierr);

  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &feAux[1]);CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe[1], &q);CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(feAux[1], q);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) feAux[1], "vorticity_0");CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &feAux[2]);CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe[2], &q);CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(feAux[2], q);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) feAux[2], "flux_0");CHKERRQ(ierr);

  /* Set discretization and boundary conditions for each mesh */
  ierr = DMCreateDS(dm); CHKERRQ(ierr);
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  for (f = 0; f < Nf; ++f) {ierr = PetscDSSetDiscretization(prob, f, (PetscObject) fe[f]);CHKERRQ(ierr);}
  ierr = PetscDSCreate(comm, &probAux);CHKERRQ(ierr);
  for (f = 0; f < NfAux; ++f) {ierr = PetscDSSetDiscretization(probAux, f, (PetscObject) feAux[f]);CHKERRQ(ierr);}
  ierr = SetupProblem(prob, ctx);CHKERRQ(ierr);
  while (cdm) {
    DM coordDM, dmAux;
    ierr = DMGetCoordinateDM(cdm,&coordDM);CHKERRQ(ierr);
    {
      PetscBool hasLabel;

      ierr = DMHasLabel(cdm, "marker", &hasLabel);CHKERRQ(ierr);
      if (!hasLabel) {ierr = CreateBCLabel(cdm, "marker");CHKERRQ(ierr);}
    }

    ierr = DMClone(cdm, &dmAux);CHKERRQ(ierr);
    ierr = DMSetCoordinateDM(dmAux, coordDM);CHKERRQ(ierr);
    /* set aux fields stuff here */
    ierr = DMSetField(dmAux, 0, NULL, (PetscObject) feAux[0]);CHKERRQ(ierr);
    ierr = DMSetField(dmAux, 1, NULL, (PetscObject) feAux[1]);CHKERRQ(ierr);
    ierr = DMSetField(dmAux, 2, NULL, (PetscObject) feAux[2]);CHKERRQ(ierr);
    ierr = DMCreateDS(dmAux); CHKERRQ(ierr);
    /* give it to the DM */
    ierr = PetscObjectCompose((PetscObject) cdm, "dmAux", (PetscObject) dmAux);CHKERRQ(ierr);
    ierr = SetupEquilibriumFields(cdm, dmAux, ctx);CHKERRQ(ierr);
    ierr = DMDestroy(&dmAux);CHKERRQ(ierr);
    /* next */
    ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  for (f = 0; f < Nf; ++f) {ierr = PetscFEDestroy(&fe[f]);CHKERRQ(ierr);}
  for (f = 0; f < NfAux; ++f) {ierr = PetscFEDestroy(&feAux[f]);CHKERRQ(ierr);}
  ierr = PetscDSDestroy(&probAux);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  DM             dm;
  TS             ts;
  Vec            u, r;
  AppCtx         ctx;
  PetscReal      t       = 0.0;
  PetscReal      L2error = 0.0;
  PetscErrorCode ierr;
  AppCtx         *ctxarr[]={&ctx,&ctx,&ctx,&ctx,&ctx}; //each variable could have a different context
  PetscMPIInt    rank;
  s_ctx = &ctx;
  ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr = ProcessOptions(PETSC_COMM_WORLD, &ctx);CHKERRQ(ierr);
  /* create mesh and problem */
  ierr = CreateMesh(PETSC_COMM_WORLD, &ctx, &dm);CHKERRQ(ierr);
  ierr = DMView(dm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(dm, &ctx);CHKERRQ(ierr);
  ierr = PetscMalloc1(MHD_NUM_COMP, &ctx.initialFuncs);CHKERRQ(ierr);
  ierr = SetupDiscretization(dm, &ctx);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) u, "u");CHKERRQ(ierr);
  ierr = VecDuplicate(u, &r);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) r, "r");CHKERRQ(ierr);
  /* create TS */
  ierr = TSCreate(PETSC_COMM_WORLD, &ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts, dm);CHKERRQ(ierr);
  ierr = TSSetApplicationContext(ts, &ctx);CHKERRQ(ierr);
  ierr = DMTSSetBoundaryLocal(dm, DMPlexTSComputeBoundary, &ctx);CHKERRQ(ierr);
  if (ctx.implicit) {
    ierr = DMTSSetIFunctionLocal(dm, DMPlexTSComputeIFunctionFEM, &ctx);CHKERRQ(ierr);
    ierr = DMTSSetIJacobianLocal(dm, DMPlexTSComputeIJacobianFEM, &ctx);CHKERRQ(ierr);
  } else {
    ierr = DMTSSetRHSFunctionLocal(dm, DMPlexTSComputeRHSFunctionFVM, &ctx);CHKERRQ(ierr);
  }
  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetPostStep(ts, PostStep);CHKERRQ(ierr);
  ierr = TSSetPreStep(ts, PreStep);CHKERRQ(ierr);
  /* make solution & solve */
  ierr = DMProjectFunction(dm, t, ctx.initialFuncs, (void **)ctxarr, INSERT_ALL_VALUES, u);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,u);CHKERRQ(ierr);
  ierr = TSSetErrorIfStepFails(ts, PETSC_FALSE);CHKERRQ(ierr);
  ierr = TSSolve(ts, u);CHKERRQ(ierr);
  ierr = TSGetTime(ts, &t);CHKERRQ(ierr);
  ierr = DMComputeL2Diff(dm, t, ctx.initialFuncs, (void **)ctxarr, u, &L2error);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Diff: %g at time %g\n",L2error,t);CHKERRQ(ierr);

#if 0
  {
    PetscReal res = 0.0;
    /* Check discretization error */
    ierr = VecViewFromOptions(u, NULL, "-initial_guess_view");CHKERRQ(ierr);
    ierr = DMComputeL2Diff(dm, 0.0, ctx.exactFuncs, NULL, u, &error);CHKERRQ(ierr);
    if (error < 1.0e-11) {ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Error: < 1.0e-11\n");CHKERRQ(ierr);}
    else                 {ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Error: %g\n", error);CHKERRQ(ierr);}
    /* Check residual */
    ierr = SNESComputeFunction(snes, u, r);CHKERRQ(ierr);
    ierr = VecChop(r, 1.0e-10);CHKERRQ(ierr);
    ierr = VecViewFromOptions(r, NULL, "-initial_residual_view");CHKERRQ(ierr);
    ierr = VecNorm(r, NORM_2, &res);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Residual: %g\n", res);CHKERRQ(ierr);
    /* Check Jacobian */
    {
      Mat A;
      Vec b;

      ierr = SNESGetJacobian(snes, &A, NULL, NULL, NULL);CHKERRQ(ierr);
      ierr = SNESComputeJacobian(snes, u, A, A);CHKERRQ(ierr);
      ierr = VecDuplicate(u, &b);CHKERRQ(ierr);
      ierr = VecSet(r, 0.0);CHKERRQ(ierr);
      ierr = SNESComputeFunction(snes, r, b);CHKERRQ(ierr);
      ierr = MatMult(A, u, r);CHKERRQ(ierr);
      ierr = VecAXPY(r, 1.0, b);CHKERRQ(ierr);
      ierr = VecDestroy(&b);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Au - b = Au + F(0)\n");CHKERRQ(ierr);
      ierr = VecChop(r, 1.0e-10);CHKERRQ(ierr);
      ierr = VecViewFromOptions(r, NULL, "-linear_residual_view");CHKERRQ(ierr);
      ierr = VecNorm(r, NORM_2, &res);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Linear L_2 Residual: %g\n", res);CHKERRQ(ierr);
    }
  }
#endif
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFree(ctx.initialFuncs);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
