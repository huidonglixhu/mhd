static char help[] = "View null space of bracket operator.\n\n\n";

/*F
This is a three field model for the density $\tilde n$, vorticity $\tilde\Omega$ ...
F*/

#include <petscdmplex.h>
#include <petscts.h>
#include <petscds.h>
#include <assert.h>
//#include <slepceps.h>

typedef struct {
  PetscInt       debug;             /* The debugging level */
  /* Domain and mesh definition */
  PetscInt       dim;               /* The topological mesh dimension */
  char           filename[2048];    /* The optional ExodusII file */
  PetscBool      cell_simplex;           /* Simplicial mesh */
  DMBoundaryType boundary_types[3];
  PetscInt       cells[3];
  PetscInt       refine;
  /* geometry  */
  PetscReal      domain_lo[3], domain_hi[3];
  DMBoundaryType periodicity[3];              /* The domain periodicity */
  PetscReal      b0[3]; /* not used */
  /* Problem definition */
  PetscErrorCode (**initialFuncs)(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx);
  PetscReal      mu, eta, beta, ves;
  PetscReal      Jo,Jop,m,ke,kx,ky,DeltaPrime,eps;
  /* solver */
  PetscBool      implicit;
  PetscReal      dt; /* initial time step to not go over */
  PetscReal      dt_reduction_factor;
  PetscInt       iteration_dt_trigger;
} AppCtx;

static AppCtx *s_ctx;
static PetscReal s_K[2][2] = {{0,1},{-1,0}};

enum field_idx {NDEN,OMEGA,NUM_FIELDS};
/*
*/
static void g0_dt(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[])
{
  g0[0] = u_tShift;
}

/* 'right' Poisson bracket -< . , phi0>, live var is left, held var is right */
static void g1_nden_right(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                          const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                          const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                          PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pDenDer = &u_x[uOff_x[NDEN]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[i] += -s_K[i][j]*pDenDer[j];
}

/* 'left' Poisson bracket - < Omega , . >, live var is right, held var is left */
static void g1_nomega_left(PetscInt dim, PetscInt Nf, PetscInt NfAux,
			   const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
			   const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
			   PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt          i,j;
  const PetscScalar *pOmegaDer   = &u_x[uOff_x[OMEGA]];
  for (i = 0; i < dim; ++i)
    for (j = 0; j < dim; ++j)
      g1[j] += -pOmegaDer[i]*s_K[i][j];
}

static void g3_nmu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) {
    g3[d*dim+d] = -s_ctx->mu;
  }
}


/* residual point methods */
static PetscScalar poissonBracket(PetscInt dim, const PetscScalar df[], const PetscScalar dg[])
{
  PetscScalar ret = df[0]*dg[1] - df[1]*dg[0];
  /* if (dim==3) { */
  /*   ret += df[1]*dg[2] - df[2]*dg[1]; */
  /*   ret += df[2]*dg[0] - df[0]*dg[2]; */
  /* } */
  return ret;
}

static void f1_nmu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscScalar *pnDer = &u_x[uOff_x[NDEN]];
  PetscInt           d;
  for (d = 0; d < 2; ++d) f1[d] = - s_ctx->mu*pnDer[d];
}

static void f0_Omega(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                     const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                     const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                     PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  const PetscScalar *pOmegaDer = &u_x[uOff_x[OMEGA]];
  const PetscScalar *pDenDer   = &u_x[uOff_x[NDEN]];

  f0[0] += - poissonBracket(dim, pOmegaDer, pDenDer);
  if (u_t) f0[0] += u_t[OMEGA];
}

static void f0_n(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  f0[0] = -1000;
  if (u_t) f0[0] += u_t[NDEN];
}

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscBool      flg;
  PetscErrorCode ierr;
  PetscInt       ii, bd;
  PetscReal      a,b;
  PetscFunctionBeginUser;
  options->debug               = 1;
  options->dim                 = 2;
  options->filename[0]         = '\0';
  options->cell_simplex        = PETSC_FALSE;
  options->refine              = 2;
  options->domain_lo[0]  = 0.0;
  options->domain_lo[1]  = 0.0;
  options->domain_lo[2]  = 0.0;
  options->domain_hi[0]  = 2.0;
  options->domain_hi[1]  = 2.0;
  options->domain_hi[2]  = 2.0;
  options->periodicity[0]= DM_BOUNDARY_NONE;
  options->periodicity[1]= DM_BOUNDARY_NONE;
  options->periodicity[2]= DM_BOUNDARY_NONE;
  options->mu   = .01;
  options->eta  = 0.001;
  options->beta = 0.1;
  options->ves = 0.005;
  options->Jop = 0.0;
  options->m = 1;
  options->eps = 1.e-6;
  options->dt = 1;
  options->dt_reduction_factor = 4;
  options->iteration_dt_trigger = 4;

  ierr = PetscOptionsBegin(comm, "", "Poisson Problem Options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-debug", "The debugging level", "nullspace.c", options->debug, &options->debug, NULL);CHKERRQ(ierr);
   ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "nullspace.c", options->dim, &options->dim, NULL);CHKERRQ(ierr);
  for (ii = 0; ii < options->dim; ++ii) options->cells[ii] = 2;
  ierr = PetscOptionsInt("-dm_refine", "Hack to get refinement level for cylinder", "nullspace.c", options->refine, &options->refine, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_dt", "initial time step", "nullspace.c", options->dt, &options->dt, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-dt_reduction_factor", "Time step reduction factor", "nullspace.c", options->dt_reduction_factor, &options->dt_reduction_factor, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-iteration_dt_trigger", "Number of non-linear iterations to trigger time step reduction", "nullspace.c", options->iteration_dt_trigger, &options->iteration_dt_trigger, NULL);CHKERRQ(ierr);

  ierr = PetscOptionsReal("-mu", "mu", "nullspace.c", options->mu, &options->mu, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-eta", "eta", "nullspace.c", options->eta, &options->eta, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-beta", "beta", "nullspace.c", options->beta, &options->beta, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ves", "ves", "nullspace.c", options->ves, &options->ves, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-Jop", "Jop", "nullspace.c", options->Jop, &options->Jop, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-m", "m", "nullspace.c", options->m, &options->m, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-eps", "eps", "nullspace.c", options->eps, &options->eps, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-f", "Exodus.II filename to read", "nullspace.c", options->filename, options->filename, sizeof(options->filename), &flg);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-cell_simplex", "Simplicial (true) or tensor (false) mesh", "nullspace.c", options->cell_simplex, &options->cell_simplex, NULL);CHKERRQ(ierr);
  ii = options->dim;
  ierr = PetscOptionsRealArray("-domain_hi", "Domain size", "nullspace.c", options->domain_hi, &ii, NULL);CHKERRQ(ierr);
  ii = options->dim;
  ierr = PetscOptionsRealArray("-domain_lo", "Domain size", "nullspace.c", options->domain_lo, &ii, NULL);CHKERRQ(ierr);
  ii = options->dim;
  bd = options->periodicity[0];
  ierr = PetscOptionsEList("-x_periodicity", "The x-boundary periodicity", "nullspace.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[0]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[0] = (DMBoundaryType) bd;
  bd = options->periodicity[1];
  ierr = PetscOptionsEList("-y_periodicity", "The y-boundary periodicity", "nullspace.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[1]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[1] = (DMBoundaryType) bd;
  bd = options->periodicity[2];
  ierr = PetscOptionsEList("-z_periodicity", "The z-boundary periodicity", "nullspace.c", DMBoundaryTypes, 5, DMBoundaryTypes[options->periodicity[2]], &bd, NULL);CHKERRQ(ierr);
  options->periodicity[2] = (DMBoundaryType) bd;
  ii = options->dim;
  ierr = PetscOptionsIntArray("-cells", "Number of cells in each dimension", "nullspace.c", options->cells, &ii, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  a = (options->domain_hi[0]-options->domain_lo[0])/2.0;
  b = (options->domain_hi[1]-options->domain_lo[1])/2.0;
  for (ii = 0; ii < options->dim; ++ii) {
    if (options->domain_hi[ii] <= options->domain_lo[ii]) SETERRQ3(comm,PETSC_ERR_ARG_WRONG,"Domain %D lo=%g hi=%g",ii,options->domain_lo[ii],options->domain_hi[ii]);
  }
  options->ke = PetscSqrtScalar(options->Jop);
  if (options->Jop==0.0) {
    options->Jo = 1.0/PetscPowScalar(a,2);
  } else {
    options->Jo = options->Jop*PetscCosReal(options->ke*a)/(1.0-PetscCosReal(options->ke*a));
  }
  options->ky = PETSC_PI*options->m/b;
  if (PetscPowScalar(options->ky,2)<options->Jop) {
    options->kx = PetscSqrtScalar(options->Jop-PetscPowScalar(options->ky,2));
    options->DeltaPrime = -2.0*options->kx*a*PetscCosReal(options->kx*a)/PetscSinReal(options->kx*a);
  } else if (PetscPowScalar(options->ky,2)>options->Jop) {
    options->kx = PetscSqrtScalar(PetscPowScalar(options->ky,2)-options->Jop);
    options->DeltaPrime = -2.0*options->kx*a*PetscCoshReal(options->kx*a)/PetscSinhReal(options->kx*a);
  } else { //they're equal (or there's a NaN), lim(x*cot(x))_x->0=1
    options->kx = 0;
    options->DeltaPrime = -2.0;
  }
  ierr = PetscPrintf(comm, "Jop=%g\n",options->Jop);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "DeltaPrime=%g\n",options->DeltaPrime);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "eta=%g\n",options->eta);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "beta=%g\n",options->beta);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "mu=%g\n",options->mu);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "ves=%g\n",options->ves);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "f_n"
static void f_n(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar *f0)
{
  const PetscScalar *pn = &u[uOff[NDEN]];
  *f0 = *pn;
}

#undef __FUNCT__
#define __FUNCT__ "PreStep"
static PetscErrorCode PreStep(TS ts)
{
  PetscErrorCode    ierr;
  DM                dm;
  AppCtx            *ctx;
  PetscInt          num;
  Vec               X;
  PetscReal         val;
  PetscFunctionBegin;
  ierr = TSGetApplicationContext(ts, &ctx);CHKERRQ(ierr); assert(ctx);
  if (ctx->debug<1) PetscFunctionReturn(0);
  ierr = TSGetSolution(ts, &X);CHKERRQ(ierr);
  ierr = VecGetDM(X, &dm);CHKERRQ(ierr);
  ierr = DMGetOutputSequenceNumber(dm, &num, &val);CHKERRQ(ierr);
  if (num < 0) {ierr = DMSetOutputSequenceNumber(dm, 0, 0.0);CHKERRQ(ierr);}
  if (num < 1) { /* initial state only */
    ierr = PetscObjectSetName((PetscObject) X, "u");CHKERRQ(ierr);
    ierr = VecViewFromOptions(X, NULL, "-vec_view");CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PostStep"
static PetscErrorCode PostStep(TS ts)
{
  PetscErrorCode      ierr;
  DM                  dm;
  AppCtx              *ctx;
  PetscInt            stepi,num;
  Vec                 X;
  PetscDS             prob;
  DM                  plex;
  PetscScalar         den, tt[5];
  PetscReal           val,time;
  SNES                snes;
  SNESConvergedReason reason;
  PetscFunctionBegin;
  ierr = TSGetApplicationContext(ts, &ctx);CHKERRQ(ierr); assert(ctx);
  if (ctx->debug<1) PetscFunctionReturn(0);
  ierr = TSGetSNES(ts,&snes);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes,&reason);CHKERRQ(ierr);
  if (reason<0) {
    PetscPrintf(PetscObjectComm((PetscObject)ts), "\t\t% PostStep: SNES diverged with reason %D.\n",reason);CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }
  ierr = TSGetSolution(ts, &X);CHKERRQ(ierr);
  ierr = VecGetDM(X, &dm);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) X, "u");CHKERRQ(ierr);
  ierr = DMGetOutputSequenceNumber(dm, &num, &val);CHKERRQ(ierr);
  ierr = TSGetTime(ts, &time);CHKERRQ(ierr);
  ierr = DMSetOutputSequenceNumber(dm, ++num, time);CHKERRQ(ierr); /* need this to keep ahead of initial state */
  ierr = VecViewFromOptions(X, NULL, "-vec_view");CHKERRQ(ierr);
  /* print integrals */
  ierr = DMConvert(dm, DMPLEX, &plex);CHKERRQ(ierr);
  ierr = DMGetDS(plex, &prob);CHKERRQ(ierr);
  ierr = PetscDSSetObjective(prob, 0, &f_n);CHKERRQ(ierr);
  ierr = DMPlexComputeIntegralFEM(plex,X,tt,ctx);CHKERRQ(ierr);
  den = tt[0];
  ierr = TSGetStepNumber(ts, &stepi);CHKERRQ(ierr);
  /* clean up */
  ierr = DMDestroy(&plex);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateBCLabel(DM dm, const char name[])
{
  DMLabel        label;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = DMCreateLabel(dm, name);CHKERRQ(ierr);
  ierr = DMGetLabel(dm, name, &label);CHKERRQ(ierr);
  ierr = DMPlexMarkBoundaryFaces(dm, PETSC_DETERMINE, label);CHKERRQ(ierr);
  ierr = DMPlexLabelComplete(dm, label);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateMesh(MPI_Comm comm, AppCtx *ctx, DM *dm)
{
  PetscInt       dim      = ctx->dim;
  const char    *filename = ctx->filename;
  size_t         len;
  PetscMPIInt    numProcs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_size(comm, &numProcs);CHKERRQ(ierr);
  ierr = PetscStrlen(filename, &len);CHKERRQ(ierr);
  if (len) {
    ierr = DMPlexCreateFromFile(comm, filename, PETSC_TRUE, dm);CHKERRQ(ierr);
  } else {
    PetscInt d,i;
    /* create DM */
    if (ctx->cell_simplex && dim == 3) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot mesh a cylinder with simplices");
    if (dim==2) {
      PetscInt npi, totCells = 1, nrefine = 0;
      char     str[8];
      if (ctx->cell_simplex) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot mesh 2D with simplices");
      npi = PetscMax((PetscInt) (PetscPowReal(numProcs, 1.0/dim) + 0.001), 1);
      for (d = 0; d < dim; ++d) {
        if (ctx->periodicity[d]==DM_BOUNDARY_PERIODIC && ctx->cells[d]*npi < 3) {
	  for (i = 0; i < dim; ++i) ctx->cells[i] *= 2;
	  nrefine++;
	}
	while (ctx->cells[d] < npi) {
	  for (i = 0; i < dim; ++i) ctx->cells[i] *= 2;
	  nrefine++;
	}
      }
      for (d = 0; d < dim; ++d) totCells *= ctx->cells[d];
      if (totCells < numProcs) SETERRQ5(comm,PETSC_ERR_ARG_WRONG,"Total cells %D less than number of processes %D, %D x %D cell grid, npi: %D", totCells, numProcs,ctx->cells[0],ctx->cells[1], npi);
      ierr = PetscPrintf(comm, "DMPlexCreateBoxMesh with %D total cells, %D x %D cell grid, with %D refinements\n",totCells,ctx->cells[0],ctx->cells[1],ctx->refine-nrefine);CHKERRQ(ierr);
      ierr = PetscSNPrintf(str, 8, "%D", ctx->refine - nrefine);CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-dm_refine");CHKERRQ(ierr);
      ierr = PetscOptionsSetValue(NULL,"-dm_refine",str);CHKERRQ(ierr);
      ierr = DMPlexCreateBoxMesh(comm, dim, PETSC_FALSE, ctx->cells, ctx->domain_lo, ctx->domain_hi, ctx->periodicity, PETSC_TRUE, dm);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) *dm, "Box Mesh");CHKERRQ(ierr);
    } else {
      if (ctx->periodicity[0]==DM_BOUNDARY_PERIODIC || ctx->periodicity[1]==DM_BOUNDARY_PERIODIC) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot do periodic in x or y in a cylinder");
      /* we stole dm_refine so clear it */
      ierr = PetscOptionsClearValue(NULL,"-dm_refine");CHKERRQ(ierr);
      ierr = DMPlexCreateHexCylinderMesh(comm, ctx->refine, ctx->periodicity[2], dm);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) *dm, "Cylinder Mesh");CHKERRQ(ierr);
    }
  }
  {
    DM               distributedMesh = NULL;
    PetscPartitioner part;
    ierr = DMPlexGetPartitioner(*dm,&part);CHKERRQ(ierr);
    ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
    /* Distribute mesh over processes */
    ierr = DMPlexDistribute(*dm, 0, NULL, &distributedMesh);CHKERRQ(ierr);
    if (distributedMesh) {
      ierr = DMDestroy(dm);CHKERRQ(ierr);
      *dm  = distributedMesh;
    }
  }
  {
    PetscBool hasLabel;
    ierr = DMHasLabel(*dm, "marker", &hasLabel);CHKERRQ(ierr);
    if (!hasLabel) {ierr = CreateBCLabel(*dm, "marker");CHKERRQ(ierr);}
  }
  {
    char      convType[256];
    PetscBool flg;
    ierr = PetscOptionsBegin(comm, "", "Mesh conversion options", "DMPLEX");CHKERRQ(ierr);
    ierr = PetscOptionsFList("-dm_plex_convert_type","Convert DMPlex to another format","mhd",DMList,DMPLEX,convType,256,&flg);CHKERRQ(ierr);
    ierr = PetscOptionsEnd();
    if (flg) {
      DM dmConv;
      ierr = DMConvert(*dm,convType,&dmConv);CHKERRQ(ierr);
      if (dmConv) {
        ierr = DMDestroy(dm);CHKERRQ(ierr);
        *dm  = dmConv;
      }
    }
  }
  ierr = DMLocalizeCoordinates(*dm);CHKERRQ(ierr); /* needed for periodic */
  ierr = DMSetFromOptions(*dm);CHKERRQ(ierr);

  ierr = DMViewFromOptions(*dm, NULL, "-dm_view");CHKERRQ(ierr);

  if (/* user->viewHierarchy */ 0 ) {
    DM       cdm = *dm;
    PetscInt i   = 0;
    char     buf[256];
    
    while (cdm) {
      ierr = DMSetUp(cdm);CHKERRQ(ierr);
      ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
      ++i;
    }
    cdm = *dm;
    while (cdm) {
      PetscViewer       viewer;
      PetscBool   isHDF5, isVTK;
      --i;
      ierr = PetscViewerCreate(comm,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERHDF5);CHKERRQ(ierr);
      ierr = PetscViewerSetOptionsPrefix(viewer,"hierarchy_");CHKERRQ(ierr);
      ierr = PetscViewerSetFromOptions(viewer);CHKERRQ(ierr);
      ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERHDF5,&isHDF5);CHKERRQ(ierr);
      ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERVTK,&isVTK);CHKERRQ(ierr);
      if (isHDF5) {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d.h5", i);CHKERRQ(ierr);
      }
      else if (isVTK) {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d.vtu", i);CHKERRQ(ierr);
        ierr = PetscViewerPushFormat(viewer,PETSC_VIEWER_VTK_VTU);CHKERRQ(ierr);
      }
      else {
        ierr = PetscSNPrintf(buf, 256, "ex12-%d", i);CHKERRQ(ierr);
      }
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,buf);CHKERRQ(ierr);
      ierr = DMView(cdm, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode initialSolution_n(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  AppCtx *options = (AppCtx *)ctx;
  PetscReal Ly = options->domain_hi[1]-options->domain_lo[1], yy = x[1]-options->domain_lo[1];
  PetscReal Lx = options->domain_hi[0]-options->domain_lo[0], xx = x[0]-options->domain_lo[0];
  u[0] = (yy*Ly - yy*yy) * (xx*Lx - xx*xx) * (x[0] + x[1]);
  return 0;
}

static PetscErrorCode initialSolution_Omega(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  AppCtx *options = (AppCtx *)ctx;
  PetscReal Ly = options->domain_hi[1]-options->domain_lo[1], yy = x[1]-options->domain_lo[1];
  PetscReal Lx = options->domain_hi[0]-options->domain_lo[0], xx = x[0]-options->domain_lo[0];
  u[0] = (yy*Ly - yy*yy) * (xx*Lx - xx*xx) * (x[0] - x[1]);
  return 0;
}

static PetscErrorCode SetupProblem(PetscDS prob, AppCtx *ctx)
{
  const PetscInt id = 1;
  PetscErrorCode ierr, f;

  PetscFunctionBeginUser;
  ierr = PetscDSSetJacobian(prob, NDEN, NDEN, g0_dt, NULL,  NULL, g3_nmu);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, OMEGA, NDEN, NULL, g1_nomega_left,  NULL, NULL);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, OMEGA, OMEGA, g0_dt, g1_nden_right,  NULL, NULL);CHKERRQ(ierr);

  ierr = PetscDSSetResidual(prob, NDEN,  f0_n,     f1_nmu);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, OMEGA, f0_Omega, NULL);CHKERRQ(ierr);

  ctx->initialFuncs[0] = initialSolution_n;
  ctx->initialFuncs[1] = initialSolution_Omega;
  for (f = 0; f < 2; ++f) {
    ierr = PetscDSSetImplicit( prob, f, PETSC_TRUE);CHKERRQ(ierr);
    ierr = PetscDSAddBoundary( prob, DM_BC_ESSENTIAL, "wall", "marker", f, 0, NULL, (void (*)()) ctx->initialFuncs[f], 1, &id, ctx);CHKERRQ(ierr);
  }
  ierr = PetscDSSetContext(prob, 0, ctx);CHKERRQ(ierr);
  ierr = PetscDSSetFromOptions(prob);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupDiscretization(DM dm, AppCtx *ctx)
{
  DM              cdm = dm;
  const PetscInt  dim = ctx->dim;
  PetscFE         fe[2];
  PetscDS         prob;
  PetscInt        Nf = 2, f;
  PetscBool       cell_simplex = ctx->cell_simplex;
  PetscErrorCode  ierr;
  MPI_Comm        comm = PetscObjectComm((PetscObject) dm);
  
  PetscFunctionBeginUser;
  /* Create finite element */
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[0]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[0], "density");CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, 1, cell_simplex, NULL, -1, &fe[1]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[1], "vorticity");CHKERRQ(ierr);
 
  /* Set discretization and boundary conditions for each mesh */
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  for (f = 0; f < Nf; ++f) {ierr = PetscDSSetDiscretization(prob, f, (PetscObject) fe[f]);CHKERRQ(ierr);}
  ierr = SetupProblem(prob, ctx);CHKERRQ(ierr);
  while (cdm) {
    DM coordDM;
    ierr = DMSetDS(cdm,prob);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(cdm,&coordDM);CHKERRQ(ierr);
    {
      PetscBool hasLabel;
      ierr = DMHasLabel(cdm, "marker", &hasLabel);CHKERRQ(ierr);
      if (!hasLabel) {ierr = CreateBCLabel(cdm, "marker");CHKERRQ(ierr);}
    }
    ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  for (f = 0; f < Nf; ++f) {ierr = PetscFEDestroy(&fe[f]);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}
static PetscErrorCode PrintEigs(TS ts, Vec xr, Vec xi)
/* Check Jacobian */
{
  Mat  A;
  SNES snes;
  /* EPS  eps;  */        /* eigenproblem solver context */
  PetscInt nconv,i;
  PetscScalar    vr,vi;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = TSGetSNES(ts,&snes);CHKERRQ(ierr);
  ierr = SNESGetJacobian(snes, &A, NULL, NULL, NULL);CHKERRQ(ierr);
  ierr = SNESComputeJacobian(snes, xr, A, A);CHKERRQ(ierr);
  /* plot kernels */
  /* /\* */
  /*   Create eigensolver context */
  /* *\/ */
  /* ierr = EPSCreate(PETSC_COMM_WORLD,&eps);CHKERRQ(ierr); */
  /* /\* */
  /*   Set operators. In this case, it is a standard eigenvalue problem */
  /* *\/ */
  /* ierr = EPSSetOperators(eps,A,NULL);CHKERRQ(ierr); */
  /* ierr = EPSSetProblemType(eps,EPS_NHEP);CHKERRQ(ierr);	 */
  /* /\* */
  /*   Set solver parameters at runtime */
  /* *\/ */
  /* ierr = EPSSetFromOptions(eps);CHKERRQ(ierr); */
  /* /\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*    Solve the eigensystem */
  /*    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\/ */
  /* ierr = EPSSolve(eps);CHKERRQ(ierr); */
  /* /\* */
  /*   Save eigenvectors, if requested */
  /* *\/ */
  /* ierr = EPSGetConverged(eps,&nconv);CHKERRQ(ierr); */
  /* ierr = PetscPrintf(PETSC_COMM_WORLD, "%D eigen vectors computed\n",nconv);CHKERRQ(ierr); */
  /* if (nconv>0) { */
  /*   for (i=0;i<nconv;i++) { */
  /*     ierr = EPSGetEigenvector(eps,i,xr,xi);CHKERRQ(ierr); */
  /*     ierr = EPSGetEigenvalue(eps,i,&vr,&vi);CHKERRQ(ierr); */
  /*     ierr = PetscPrintf(PETSC_COMM_WORLD, "Kernel eigen value = ( %g, %g )\n",vr,vi);CHKERRQ(ierr); */
  /*     ierr = VecViewFromOptions(xr, NULL, "-kernel_vec_view");CHKERRQ(ierr); */
  /*   } */
  /* } */
  /* ierr = EPSDestroy(&eps);CHKERRQ(ierr); */
  ierr = MatViewFromOptions(A, NULL, "-mat_view");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  DM             dm;
  TS             ts;
  Vec            u, r;
  AppCtx         ctx;
  PetscReal      t       = 0.0;
  PetscReal      L2error = 0.0;
  PetscErrorCode ierr;
  AppCtx         *ctxarr[]={&ctx,&ctx,&ctx,&ctx,&ctx}; //each variable could have a different context
  PetscMPIInt    rank;
  s_ctx = &ctx;
  ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr = ProcessOptions(PETSC_COMM_WORLD, &ctx);CHKERRQ(ierr);
  /* create mesh and problem */
  ierr = CreateMesh(PETSC_COMM_WORLD, &ctx, &dm);CHKERRQ(ierr);
  ierr = DMView(dm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(dm, &ctx);CHKERRQ(ierr);
  ierr = PetscMalloc1(2, &ctx.initialFuncs);CHKERRQ(ierr);
  ierr = SetupDiscretization(dm, &ctx);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) u, "u");CHKERRQ(ierr);
  ierr = VecDuplicate(u, &r);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) r, "r");CHKERRQ(ierr);
  /* create TS */
  ierr = TSCreate(PETSC_COMM_WORLD, &ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts, dm);CHKERRQ(ierr);
  ierr = TSSetApplicationContext(ts, &ctx);CHKERRQ(ierr);
  ierr = DMTSSetBoundaryLocal(dm, DMPlexTSComputeBoundary, &ctx);CHKERRQ(ierr);

  ierr = DMTSSetIFunctionLocal(dm, DMPlexTSComputeIFunctionFEM, &ctx);CHKERRQ(ierr);
  ierr = DMTSSetIJacobianLocal(dm, DMPlexTSComputeIJacobianFEM, &ctx);CHKERRQ(ierr);

  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetPostStep(ts, PostStep);CHKERRQ(ierr);
  ierr = TSSetPreStep(ts, PreStep);CHKERRQ(ierr);
  /* make solution & solve */
  ierr = DMProjectFunction(dm, t, ctx.initialFuncs, (void **)ctxarr, INSERT_ALL_VALUES, u);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,u);CHKERRQ(ierr);
  ierr = TSSetErrorIfStepFails(ts, PETSC_FALSE);CHKERRQ(ierr);

  ierr = TSSolve(ts, u);CHKERRQ(ierr);
  ierr = TSGetTime(ts, &t);CHKERRQ(ierr);
  ierr = DMComputeL2Diff(dm, t, ctx.initialFuncs, (void **)ctxarr, u, &L2error);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Diff: %g at time %g\n",L2error,t);CHKERRQ(ierr);

  /* ierr = PrintEigs( ts, u, r);CHKERRQ(ierr); */
  
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFree(ctx.initialFuncs);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
